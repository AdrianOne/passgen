#include <iostream>
#include <vector>
#include <random>
#include <chrono>
#include <ctime>

class Range {
public:
    Range(int s, int e) {
        start = s;
        end = e;
    }
    
    int start;
    int end;
};

void show_usage(const std::string& name) {
    std::cerr << "Usage: " << name << " <option>\n"
              << "Options: \n"
              << "\t-h, --help             | Shows this help message\n"
              << "\t-l, --length <number>  | Specifies the length of the password (Default: 8)\n"
              << "\t-p, --pattern <Aa1$>   | Specifies the pattern of the password (Default: Aa1$)"
              << std::endl;
}

bool is_integer(const std::string& s) {
    bool is_integer = true;
    // Loop trough string and set "is_integer" false if a char of the stringis not a digit
    for (char c : s) {
        if (!isdigit(c)) {
            is_integer = false;
        }
    }
    return is_integer;
}

std::vector<char> get_symbols(const std::string& pattern) {
    std::vector<char> symbols;
    
    
    // Ranges for the patterns
    Range number_range(48, 57);
    
    Range upper_alphabet_range(65, 90);
    Range lower_alphabet_range(97, 122);
    
    Range symbol_range_1(32, 47);
    Range symbol_range_2(58, 64);
    Range symbol_range_3(91, 96);
    Range symbol_range_4(123, 126);
    
    for (char c : pattern) {
        if (c == 'A') {
            for (char i = upper_alphabet_range.start; i <= upper_alphabet_range.end; i++) {
                symbols.push_back(i);
            }
        } else if (c == 'a') {
            for (char i = lower_alphabet_range.start; i <= lower_alphabet_range.end; i++) {
                symbols.push_back(i);
            }
        } else if (c == '1') {
            for (char i = number_range.start; i <= number_range.end; i++) {
                symbols.push_back(i);
            }
        } else if (c == '$') {
            for (char i = symbol_range_1.start; i <= symbol_range_1.end; i++) {
                symbols.push_back(i);
            }
            
            for (char i = symbol_range_2.start; i <= symbol_range_2.end; i++) {
                symbols.push_back(i);
            }
            
            for (char i = symbol_range_3.start; i <= symbol_range_3.end; i++) {
                symbols.push_back(i);
            }
            
            for (char i = symbol_range_4.start; i <= symbol_range_4.end; i++) {
                symbols.push_back(i);
            }
        }
    }
    return symbols;
}

char choose_random_char(const std::vector<char>& symbols) {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist(0, symbols.size() - 1);
    
    return symbols[dist(rng)];
}

std::string generate_password(int length, const std::string& pattern) {
    std::string password;
    std::vector<char> symbols = get_symbols(pattern);
    /*
     * Iterates so many times as specified with the "--length" parameter,
     * and adds a random char from the ASCII table to the password string each iteration
    */
    for (int i = 0; i < length; i++) {
        password.push_back(choose_random_char(symbols));
    }
    
    return password;
}

void show_generated_password(int length, const std::string& pattern) {
    std::string password;
    password = generate_password(length, pattern);
    std::cout << password << std::endl;
}

bool is_correct_pattern(const std::string& s) {
    bool is_correct_pattern = true; 
    for (char c : s) {
        if (c == 'A') {
            continue;
        } else if (c == 'a') {
            continue;
        } else if (c == '1') {
            continue;
        } else if (c == '$') {
            continue;
        } else {
            is_correct_pattern = false;
        }
    }
    
    return is_correct_pattern;
}

void parse_arguments(int argc, char* argv[]) {
    // "./passgen -l 10 -p A$" is currently the longest possible command possible with this program
    const int MAX_ARGUMENT_COUNT = 5;
    // "./passgen -h" is currently the shortest possbile command possible with this program
    const int MIN_ARGUMENT_COUNT = 1;
    // Default password length
    const int DEFAULT_LENGTH = 8;
    const std::string DEFAULT_PATTERN = "Aa1$";
    
    int length = DEFAULT_LENGTH;
    std::string pattern = DEFAULT_PATTERN;
    /* 
     * Checks if the submitted argument-count (argc) is in the boundaries for the program.
     * If the argument-count is outside the boundaries the usage gets shown.
    */
    if (!(argc > MAX_ARGUMENT_COUNT || argc < MIN_ARGUMENT_COUNT)) {
        for (int i = 1; i < argc; i++) {
            if (std::string(argv[i]) == "-h" || std::string(argv[i]) == "--help") {
                show_usage(argv[0]);
                return;
            } else if (std::string(argv[i]) == "-p" || std::string(argv[i]) == "--pattern") {
                /* 
                 * Checks if a value for "-p" exists, with checking if the next value isn't the last of argv
                 * ------------------------------------------
                 * Ex. passgen -p aA
                 * argc contains arguments + name with which the program was called = 3
                 * Processing -p:
                 * index i is 1
                 * -> i + 1 != argc -> true
                 * ------------------------------------------
                 * Ex. passgen -p
                 * argc contains arguments + name with which the program was called = 2
                 * Processing -p:
                 * index i is 1
                 * -> i + 1 != argc -> false -> ERROR
                */ 
                if (i + 1 != argc) {
                    // i + 1 to get the value (ex. aA) and not the argument specifier (-p)
                    if (is_correct_pattern(std::string(argv[i + 1]))) {
                        // If the value isn't "-1", the default value has been changed and the password can be generated
                        pattern = argv[i + 1];
                        if (i + 2 == argc) {
                            show_generated_password(length, pattern);
                            return;
                        } else {
                            i++;
                            continue;
                        }           
                    } else {
                        show_usage(argv[0]);
                        return;
                    }
                } else {
                    show_usage(argv[0]);
                    return;
                }     
            } else if (std::string(argv[i]) == "-l" || std::string(argv[i]) == "--length") {
                /* 
                 * Checks if a value for "-l" exists, with checking if the next value isn't the last of argv
                 * ------------------------------------------
                 * Ex. passgen -l 10
                 * argc contains arguments + name with which the program was called = 3
                 * Processing -p:
                 * index i is 1
                 * -> i + 1 != argc -> true
                 * ------------------------------------------
                 * Ex. passgen -l
                 * argc contains arguments + name with which the program was called = 2
                 * Processing -p:
                 * index i is 1
                 * -> i + 1 != argc -> false -> ERROR
                */
                if (i + 1 != argc) {
                    // i + 1 to get the value (ex. 10) and not the argument specifier (-l)
                    if (is_integer(argv[i + 1])) {
                        length = std::stoi(std::string(argv[i + 1]));
                        if (i + 2 == argc) {
                            show_generated_password(length, pattern);
                            return;
                        }
                        else {
                            i++;
                            continue;
                        }
                    } else {
                        show_usage(argv[0]);
                        return;
                    }
                } else {
                    show_usage(argv[0]);
                    return;
                }
                
            } else {
                show_usage(argv[0]);
                return;
            }
        }
        // When no arguments are passed, a default password is generated
        show_generated_password(DEFAULT_LENGTH, DEFAULT_PATTERN);
    } else {
        show_usage(argv[0]);
    }
}


int main(int argc, char* argv[]) {
    parse_arguments(argc, argv);
    return 0;
}

/*  
auto start = std::chrono::system_clock::now();
auto end = std::chrono::system_clock::now();
std::chrono::duration<double> elapsed_seconds = end-start;
std::cout << elapsed_seconds.count() * 1000 << "ms" << std::endl; 
*/
